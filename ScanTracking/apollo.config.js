module.exports = {
  client: {
    addTypename: true,
    includes: ["*"],
    name: "dashboard",
    service: {
      localSchemaFile: "schema.graphql",
      name: "saleor"
    }
  }
};