
import React from 'react';
import Login from './Login/Login';
import { createHttpLink } from 'apollo-link-http';
import { createUploadLink } from "apollo-upload-client";
import { BatchHttpLink } from "apollo-link-batch-http";
import { ErrorResponse, onError } from "apollo-link-error";
import { setContext } from "apollo-link-context";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";
import { ApolloLink } from "apollo-link";

const invalidTokenLink = onError((error) => {
  if (error.networkError && error.networkError.statusCode === 401) {
    removeAuthToken();
  }
});

const authLink = setContext((_, context) => {
  const authToken = null;

  return {
    ...context,
    headers: {
      ...context.headers,
      Authorization: authToken !== null ? `JWT ${authToken}` : null
    }
  };
});

// DON'T TOUCH THIS
// These are separate clients and do not share configs between themselves
// so we need to explicitly set them
const linkOptions = {
  credentials: "same-origin",
  uri: "https://api.annalala.vn"
};
const uploadLink = createUploadLink(linkOptions);
const batchLink = new BatchHttpLink({
  batchInterval: 100,
  ...linkOptions
});

const link = ApolloLink.split(
  operation => operation.getContext().useBatching,
  batchLink,
  uploadLink
);

const apolloClient = new ApolloClient({
  cache: new InMemoryCache({
    dataIdFromObject: (obj) => {
      // We need to set manually shop's ID, since it is singleton and
      // API does not return its ID
      if (obj.__typename === "Shop") {
        return "shop";
      }
      return defaultDataIdFromObject(obj);
    }
  }),
  link: invalidTokenLink.concat(authLink.concat(link))
});



const App = () => {
  return ( 
    <ApolloProvider client={apolloClient}>
      <Login />
    </ApolloProvider>
  );
};

export default App;
