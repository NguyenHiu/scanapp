import { ApolloError, MutationUpdaterFn } from "apollo-client";
import { DocumentNode } from "graphql";
import React from "react";
import { Mutation, MutationFunction, MutationResult } from "react-apollo";

// export function maybe(exp);
// export function maybe(exp, d); 
export function maybe(exp, d) {
  try {
    const result = exp();
    return result === undefined ? d : result;
  } catch {
    return d;
  }
};
// For some reason Mutation returns () => Element instead of () => ReactNode
export function TypedMutation(
  mutation,
  update
) {
  return (props) => {
    const { children, onCompleted, onError, variables } = props;

    return (
      <Mutation
        mutation={mutation}
        onCompleted={onCompleted}
        onError={(err) => {
          if (
            maybe(
              () =>
                err.graphQLErrors[0].extensions.exception.code ===
                "ReadOnlyException"
            )
          ) {
            console.log("No permission")
          } else {
            console.log(err)
          }
          if (onError) {
            onError(err);
          }
        }}
        variables={variables}
        update={update}
      >
        {(mutateFn, result) => (
          <>
            {children(mutateFn, {
              ...result,
              status: null
            })}
          </>
        )}
      </Mutation>
    );
  };
}
