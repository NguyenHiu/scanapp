import React from 'react';
import { TextInput, Button, Text } from 'react-native';
import {TypedTokenAuthMutation,TypedTokenVerifyMutation} from './mutations';

 const Login = () => {
  const [persistToken,setPersistToken]=React.useState(false);
  const [user , setUser]=React.useState(undefined);
  const [token, setToken] = React.useState(null);

  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");

  componentWillReceiveProps=(props)=> {
    const { tokenAuth, tokenVerify } = props;
    const tokenAuthOpts = tokenAuth[1];
    const tokenVerifyOpts = tokenVerify[1];

    if (tokenAuthOpts.error || tokenVerifyOpts.error) {
      this.logout();
    }
    if (tokenAuthOpts.data) {
      const user = tokenAuthOpts.data.tokenCreate.user;
      // FIXME: Now we set state also when auth fails and returned user is
      // `null`, because the LoginView uses this `null` to display error.
      this.setState({ user });
      if (user) {
        setAuthToken(
          tokenAuthOpts.data.tokenCreate.token,
          this.state.persistToken
        );
      }
    } else {
      if (maybe(() => tokenVerifyOpts.data.tokenVerify === null)) {
        this.logout();
      } else {
        const user = maybe(() => tokenVerifyOpts.data.tokenVerify.user);
        if (!!user) {
          this.setState({ user });
        }
      }
    }
  }
  componentDidMount=()=> {
    const { user } = this.state;
    const token = getAuthToken();
    if (!!token && !user) {
      this.verifyToken(token);
    } else {
      loginWithCredentialsManagementAPI(this.login);
    }
  }

  login = async (email, password) => {
    const { tokenAuth } = this.props;
    const [tokenAuthFn] = tokenAuth;

    tokenAuthFn({ variables: { email, password } }).then(result => {
      if (result && !result.data.tokenCreate.errors.length) {
        saveCredentials(result.data.tokenCreate.user, password);
      }
    });
  };

  loginByToken = (token, user) => {
    this.setState({ user });
    setAuthToken(token, this.state.persistToken);
  };

  logout = () => {
    this.setState({ user: undefined });
    if (isCredentialsManagementAPISupported) {
      navigator.credentials.preventSilentAccess();
    }
    removeAuthToken();
  };

  verifyToken = (token) => {
    const { tokenVerify } = this.props;
    const [tokenVerifyFn] = tokenVerify;

    return tokenVerifyFn({ variables: { token } });
  };
  return (
    <> 
      <TextInput
        style={{ borderColor: 'gray', borderWidth: 1 ,marginTop :20 }}
        value={email}
        onChangeText={
          text => setEmail(text)
        }
        textContentType="emailAddress" 
        placeholder="Your email address"
      /> 
      <TextInput
        style={{ borderColor: 'gray', borderWidth: 1 , marginTop :20 }}  
        value={password}
        onChangeText={
          text => setPassword(text)  
        }
        textContentType="password" 
        placeholder="Password"
      />
          <Button 
            title="Đăng Nhập"
            color="#2196F3"
            onPress={()=>{
              undefined
            }}
          />
      
      {token !== undefined ? ( <Text>{token}</Text> ) : ( <Text>No token</Text>)} 
    </>
  );
}; 
export default Login;
