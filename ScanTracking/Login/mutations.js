import gql from 'graphql-tag';
const tokenAuthMutation = gql`
    mutation ($email: String!, $password: String!) {
        tokenCreate(email: $email, password: $password) {
            token
            errors {
                field
                message
            }
            user {
                id
                email
                name
                permissions {
                    code
                    name
                }
                avatar {
                    url
                }
            }
        }
    }
`;
export const TypedTokenAuthMutation = TypedMutation(tokenAuthMutation);
const tokenVerifyMutation = gql`
  ${fragmentUser}
  mutation VerifyToken($token: String!) {
    tokenVerify(token: $token) {
      payload
      user {
            id
            email
            name
            permissions {
                code
                name
            }
            avatar {
                url
            }
            }
    }
  }
`;
export const TypedTokenVerifyMutation = TypedMutation(tokenVerifyMutation);